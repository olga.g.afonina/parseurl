﻿using System;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Linq;

namespace ParseUrl
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private async void button1_Click(object sender, EventArgs e)
		{
			if (!string.IsNullOrEmpty(textBox1.Text.Trim()))
			{
				var content = await GetAsync(textBox1.Text.Trim());
				var urls = Parse(content);
				if (urls != null)
					richTextBox1.Text = string.Join(Environment.NewLine, urls);
			}
		}

		private async Task<string> GetAsync(string uri)
		{
			using (var httpClient = new HttpClient())
			{
				return await Task.Run(() => httpClient.GetStringAsync(uri));
			}
		}

		private string[] Parse(string input)
		{
			try
			{
				var pattern = new Regex(@"https?://[^\s\""\']*");
				return pattern
					.Matches(input)
					.OfType<Match>()
					.Select(m => m.Value)
					.ToArray();
			}
			catch (RegexMatchTimeoutException ex)
			{
				MessageBox.Show(ex.Message);
			}
			return null;
		}
	}
}
